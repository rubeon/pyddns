# Welcome to PyDDNS!

PyDDNS is a simple script that will use Amazon Web Services's Route 53 to 
make your domain name into a groovy Dynamic DNS service.

In short, it tells your domain to point to your currrent public IP address.

Requirements:

- Python
- Boto
- Amazon-hosted Domain Name

# Usage

```bash

python pyddns.py 

```

The script is pretty simple to use. First, create the configuration file, and then
run the script. It will check [ident.me][ident] for your current IP address, then 
compare that to Route 53's record for each of the configured domain names. It will 
update those domain names accordingly.

## Configuration

The script looks for a configration file `pyddns.conf` in one of the following locations 
(in order):

- The script's local directory
- `~/.config/pyddns/`
- `/etc/pyddns/`

The configuration file is an INI-style file, should have the following
settings:

```ini

[aws]
access_key_id=YOURACCESSKEY
secret_access_key=YOURSECRET/KEY

[domains]
domain_list = mydomain.org, mydomain.net

[mydomain.org]
set_main = true

[mydomain.net]
set_main = true

```
Notes:

* The AWS credentials are stored in `[aws]`
* All that's required for each domain is `'set_main = true'`; in the future, sections may support other configuration parameters than this.

[ident]:http://ident.me/